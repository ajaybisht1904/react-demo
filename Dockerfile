FROM nginx:1.17.1-alpine
COPY default.conf /etc/nginx/conf.d/
COPY nginx.crt /etc/ssl/
COPY nginx.key /etc/ssl
COPY /build /usr/share/nginx/html